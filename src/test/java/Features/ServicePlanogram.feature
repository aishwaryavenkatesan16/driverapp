@driverAppServicePlanogramFlow
Feature: Service Planogram flow of DriverApp
      
      @servicePlanogramFlow
  Scenario Outline: Service Planogram flow validations
    Given I launch the application in "<OSVersion>" for "<Functionality>"
    Then I click on driver app to access location
    And I click on driver app to access media
    Then I enter "<username>" and "<password>"
    And I click login button
    And I verify operator text in "<OSVersion>"
    Then I click on operator
    And I click on route in "<OSVersion>"
   #Then I click on "<Date>" date
    And I extract asset ID type and place from Scheduled asset screen for "<OSVersion>"
    And I click on asset in scheduled asset screen for "<OSVersion>"
    When I click on "Pick List" icon
    Then validate if route place and type is displayed in Pick list screen for "<OSVersion>"
    And I extract coils details from Picklist screen
    And I extract coil names from Picklist screen to validate in Service Inventory screen
    And I navigate back to select activity screen from Pick list screen
    When I navigate to "Service/Inventory" screen for "<OSVersion>"
    Then I navigate back to select activity screen from Service Inv screen
    #And validate Inv mandatory error message displayed
     Then I click on "Okay" in Service Inventory screen
    And validate coil details for each coil displayed
    And validate fields dispalyed for each coil
    #Then validate if field values are "0" by default
    #And validate if "Inv" field value is empty by default
    #And validate field values of each coil is lesser than or equal to respective coil "Cap" value
    Then I navigate back to select activity screen from Service Inv screen
    Then I click on "Cancel" in Service Inventory screen
    #Then I click on "DONE" button
    And Update execution status for "<Functionality>" functionality

    Examples: 
      | OSVersion | username | password | Operator |  Route | Functionality            |
      #| iOS       | skaushik@cantaloupe.com      | Saurabh@123   | OperatorFlowFunctionality |
      | android   | skaushik@cantaloupe.com      | Saurabh@123  | Accent - Main HQ (TX) | 109 | servicePlanogramPageFunctionality |