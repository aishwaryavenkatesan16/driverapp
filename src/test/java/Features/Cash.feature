@driverAppCashFlow
Feature: Cash flow of DriverApp
      
      @cashFlow
  Scenario Outline: Cash flow validations
    Given I launch the application in "<OSVersion>" for "<Functionality>"
    Then I click on driver app to access location
    And I click on driver app to access media
    Then I enter "<username>" and "<password>"
    And I click login button
    And I verify operator text in "<OSVersion>"
    Then I click on operator
    And I click on route in "<OSVersion>"
   #Then I click on "<Date>" date
   And I extract asset ID type and place from Scheduled asset screen for "<OSVersion>"
    And I click on asset in scheduled asset screen for "<OSVersion>"
    When I click on "Cash" icon for "<OSVersion>"
    Then validate if route place and type is displayed in Cash screen
    And validate if cash headers are displayed
    Then validate the character limits of all fields
    Then I click on "DONE" button in cash screen
    And validate if "Cash" icon has "completed" mark on all fields entry
     When I click on "Cash" icon for "<OSVersion>"
     Then validate data is saved for all fields in cash page
    And Update execution status for "<Functionality>" functionality

    Examples: 
      | OSVersion | username | password | Operator |  Route | Functionality            |
      #| iOS       | skaushik@cantaloupe.com      | Saurabh@123   | OperatorFlowFunctionality |
      | android   | skaushik@cantaloupe.com      | Saurabh@123  | Accent - Main HQ (TX) | 109 | CashPageFunctionality |