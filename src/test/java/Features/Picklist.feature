@driverAppPicklistFlow
Feature: Picklist flow of DriverApp
      
      @picklistFlow
  Scenario Outline: Picklist flow validations
    Given I launch the application in "<OSVersion>" for "<Functionality>"
    Then I click on driver app to access location
    And I click on driver app to access media
    Then I enter "<username>" and "<password>"
    And I click login button
    And I verify operator text in "<OSVersion>"
    Then I click on operator
    And I click on route in "<OSVersion>"
   #Then I click on "<Date>" date
    And I extract asset ID type and place from Scheduled asset screen for "<OSVersion>"
    And I click on asset in scheduled asset screen for "<OSVersion>"
    When I click on "Pick List" icon
    Then validate if route place and type is displayed in Pick list screen for "<OSVersion>"
    #And I validate coil details display in Picklist screen
    And I extract coils details from Picklist screen
    And I extract coil names from Picklist screen to validate in Service Inventory screen
    And I navigate back to select activity screen from Pick list screen
    And validate if "Pick List" icon has "not completed" mark
    When I navigate to "Service/Inventory" screen for "<OSVersion>"
    And I click on coils and fill values in "Inv" fields
    Then I navigate back to select activity screen from Service Inv screen
     Then I click on "Cancel" in Service Inventory screen
    #And validate if "Pick list" icon has "completed" mark
    When I click on "Pick List" icon
    #And I validate price mismatch error message in Pick list screen
    And I navigate back to select activity screen from Pick list screen
    Then I click on "DONE" button
    And Update execution status for "<Functionality>" functionality

    Examples: 
      | OSVersion | username | password | Operator |  Route | Functionality            |
      #| iOS       | skaushik@cantaloupe.com      | Saurabh@123   | OperatorFlowFunctionality |
      | android   | skaushik@cantaloupe.com      | Saurabh@123  | Accent - Main HQ (TX) | 109 | PicklistPageFunctionality |