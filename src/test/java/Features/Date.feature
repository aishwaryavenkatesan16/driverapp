@driverAppDateFlow
Feature: Date flow of DriverApp
      
      @dateFlow
  Scenario Outline: Date flow validations
    Given I launch the application in "<OSVersion>" for "<Functionality>"
    Then I enter "<username>" and "<password>"
    And I click login button
    And I verify operator text in "<OSVersion>"
    Then I click on "<Operator>" operator
    And I click on "<Route>" route in "<OSVersion>"
    Then validate if Date page is displayed for "<OSVersion>"
    And validate if "Start" and "View" buttons are displayed
    #And validate if dates are displayed in ascending order
    #When I click on "Start" button
    #Then validate if Scheduled Asset page is displayed and "any" action can be performed
    #And I navigate back to Date screen
    #When I click on "View" button
    #Then validate if Scheduled Asset page is displayed and "no" action can be performed
    #And I navigate back to Date screen
    Then I click logout
    And Update execution status for "<Functionality>" functionality

    Examples: 
      | OSVersion | username | password | Operator |  Route | Functionality            |
      #| iOS       | skaushik@cantaloupe.com      | Saurabh@123   | OperatorFlowFunctionality |
      | android   | skaushik@cantaloupe.com      | Saurabh@123  | All Star Services | List items, QuickPick, List items | DateFlowFunctionality |