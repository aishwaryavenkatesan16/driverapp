package androidPages;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.testng.asserts.Assertion;
import org.testng.log4testng.Logger;

import Pages.DataRead;
import mobileWrap.MobileWrapper;
import net.bytebuddy.dynamic.scaffold.MethodRegistry.Handler.ForAbstractMethod;

public class ScheduledAssetPageAndroid extends MobileWrapper {

	DataRead data = new DataRead();
	String sheetName = "Picklist";
	Assertion assertion = new Assertion();
	public static String platform;
	public static String assetID;
	public static String type;
	public static String place;
	static List<String> coilList=new ArrayList<>();
	static List<String> coils=new ArrayList<>();

	String checkMarkXpath = "";
	String backPicklistXpath = "//android.widget.TextView[@content-desc=\"Back\"]";
	String picklistXpath = "//android.widget.TextView[@content-desc=\"Pick List\"]";
	
	String machine1XPath="(//android.widget.TextView[@content-desc=\"machine\"])[1]";
	
	String coil1XPath="(//android.widget.TextView[@content-desc=\"Coils\"])[1]";
	String coil2XPath="(//android.widget.TextView[@content-desc=\"Coils\"])[2]";
	String coil3XPath="(//android.widget.TextView[@content-desc=\"Coils\"])[3]";
	String coil4XPath="(//android.widget.TextView[@content-desc=\"Coils\"])[4]";

	/******* Test Data from Excel sheet ********/
	String assetIDData = data.getCellData(sheetName, "Asset ID", 2);
	String routeData = data.getCellData(sheetName, "Route", 2);
//	String placeData = data.getCellData(sheetName, "Place", 2);
//	String typeData = data.getCellData(sheetName, "Type", 2);
//	String coil1Data = data.getCellData(sheetName, "Coil 1", 2);
//	String coil2Data = data.getCellData(sheetName, "Coil 2", 2);
//	String coil3Data = data.getCellData(sheetName, "Coil 3", 2);
//	String coil4Data = data.getCellData(sheetName, "Coil 4", 2);
//	String dexPriceMismatchErrorMsgData = data.getCellData(sheetName, "Dex Price Mismatch Error Msg", 2);

	private static Logger LOGGER;

	public ScheduledAssetPageAndroid clickOnAsset() {
//		clickAccessibilityID(assetIDData);
		clickAccessibilityID(assetID);
		return this;
	}
	
	public ScheduledAssetPageAndroid extractAssetIDTypePlace() {
//		String machine=getText("xpath", "machine1XPath");
		String machine=getResourceIDUsingAccessibilityID(assetIDData);
		assetID=machine.split(";")[0].split(":")[1].trim();
		type=machine.split(";")[1].split(":")[1].trim();
		place=machine.split(";")[2].split(":")[1].trim();
		
		return this;
	}

	public ScheduledAssetPageAndroid clickOnElement(String icon) {
		clickAccessibilityID(icon);
		return this;
	}
	
	public List<String> getAssetIDRouteTypePlace(){
		List<String> assetIDRouteTypePlace=new ArrayList<>();
		assetIDRouteTypePlace.add(assetID);
		assetIDRouteTypePlace.add(routeData);
		assetIDRouteTypePlace.add("Type: "+type);
		assetIDRouteTypePlace.add(place);
		return assetIDRouteTypePlace;
		
	}

	

}
