package androidPages;

import mobileWrap.MobileWrapper;

public class DatePageAndroid extends MobileWrapper {

	String date = "(//android.widget.TextView[@content-desc=\"Text\"])[2]";
	String route="(//android.widget.TextView[@content-desc=\"Text\"])[6]";
	String dateText = "//android.view.ViewGroup[@content-desc=\"Title, Date, Title\"]/android.widget.TextView";
	String startBtn="(//android.view.ViewGroup[@content-desc=\"Start, Start, Start\"])[1]";
	String viewBtn="//android.view.ViewGroup[@content-desc=\"View, View, View\"]";
	
	
	//Verify the Route Name
	public DatePageAndroid VerifyRouteName(String data) {
	
	verifyText(route,"xpath", route);
	
	return this;
}
	
	
	//Verify the Date Text
			public DatePageAndroid VerifyDateText() {
			
			verifyText("Date","xpath", dateText);
			
			return this;
		}
			
			
			public DatePageAndroid verifyStartViewBtnsDsiplayed() {
				eleIsDisplayed("xpath", startBtn);
				eleIsDisplayed("xpath", viewBtn);
				return this;
			}
	

}
