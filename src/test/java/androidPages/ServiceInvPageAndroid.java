package androidPages;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.testng.asserts.Assertion;
import org.testng.log4testng.Logger;

import Pages.DataRead;
import mobileWrap.MobileWrapper;
import net.bytebuddy.dynamic.scaffold.MethodRegistry.Handler.ForAbstractMethod;

public class ServiceInvPageAndroid extends MobileWrapper {

	DataRead data = new DataRead();
	String sheetName = "ServiceInv";
	Assertion assertion = new Assertion();
	public static String platform;
	static List<String> coilNames=new ArrayList<>();
	static List<String> coilDetails=new ArrayList<>();

	String checkMarkXpath = "";
	String backServiceInvXpath = "//android.widget.TextView[@content-desc=\"Back\"]";

	/******* Test Data from Excel sheet ********/
	String coilsData = data.getCellData(sheetName, "Coils", 2);
	String invData = data.getCellData(sheetName, "InvValue", 2);
	String invMandatoryErrorMsgData = data.getCellData(sheetName, "Inv Mandatory Error Msg", 2);

	private static Logger LOGGER;

	public ServiceInvPageAndroid clickOnElement(String ele) {
		clickAccessibilityID(ele);
		return this;
	}
	

	public ServiceInvPageAndroid addValueInField(String value, String fieldName) {
		
		clickAccessibilityID(fieldName);
		clickAccessibilityID("clear");
		clickAccessibilityID(value);
		return this;
	}

	public ServiceInvPageAndroid clickOnBackBtnInServiceInv() {
		click("xpath", backServiceInvXpath);
		return this;
	}
	
	public ServiceInvPageAndroid fillInInvForAllCoils(String field) {
		coilNames=new PicklistPageAndroid().extractCoilsNames();
		for (String cl : coilNames) {
			clickAccessibilityID(cl);
			clickAccessibilityID(field);
			clickAccessibilityID("clear");
			clickAccessibilityID(invData);
		}
		
		return this;
	}
	
	public ServiceInvPageAndroid validateInvMandatoryErrorMsg() {
		eleIsDisplayedAccessibilityID(invMandatoryErrorMsgData);
		return this;
	}
	
	public ServiceInvPageAndroid validateCoilDetailsForEachCoil() {
		coilNames=new PicklistPageAndroid().extractCoilsNames();
		coilDetails=new PicklistPageAndroid().extractCoilsDetails();
		for (String cl : coilNames) {
			clickAccessibilityID(cl);
			
		}
		return this;
	}
	
	public ServiceInvPageAndroid validateFieldsDisplayedForEachCoil() {
		coilNames=new PicklistPageAndroid().extractCoilsNames();
		for (String cl : coilNames) {
			clickAccessibilityID(cl);
			eleIsDisplayedAccessibilityID("Fill");
			eleIsDisplayedAccessibilityID("Remove");
			eleIsDisplayedAccessibilityID("Spoil");
			eleIsDisplayedAccessibilityID("Inv");
		}
		
		return this;
	}

}
