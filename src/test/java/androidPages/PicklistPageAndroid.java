package androidPages;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.testng.asserts.Assertion;
import org.testng.log4testng.Logger;

import Pages.DataRead;
import mobileWrap.MobileWrapper;
import net.bytebuddy.dynamic.scaffold.MethodRegistry.Handler.ForAbstractMethod;

public class PicklistPageAndroid extends MobileWrapper {

	DataRead data = new DataRead();
	String sheetName = "Picklist";
	Assertion assertion = new Assertion();
	public static String platform;
	public static String assetID;
	public static String type;
	public static String place;
	static List<String> coilList=new ArrayList<>();
	static List<String> coils=new ArrayList<>();

	String checkMarkXpath = "";
	String backPicklistXpath = "//android.widget.TextView[@content-desc=\"Back\"]";
	String picklistXpath = "//android.widget.TextView[@content-desc=\"Pick List\"]";
	String machine1XPath="(//android.widget.TextView[@content-desc=\"machine\"])[1]";
	
	String coil1XPath="(//android.widget.TextView[@content-desc=\"Coils\"])[1]";
	String coil2XPath="(//android.widget.TextView[@content-desc=\"Coils\"])[2]";
	String coil3XPath="(//android.widget.TextView[@content-desc=\"Coils\"])[3]";
	String coil4XPath="(//android.widget.TextView[@content-desc=\"Coils\"])[4]";

	/******* Test Data from Excel sheet ********/
	String assetIDData = data.getCellData(sheetName, "Asset ID", 2);
	String routeData = data.getCellData(sheetName, "Route", 2);
//	String dexPriceMismatchErrorMsgData = data.getCellData(sheetName, "Dex Price Mismatch Error Msg", 2);

	private static Logger LOGGER;

	public PicklistPageAndroid clickOnElement(String icon) {
		clickAccessibilityID(icon);
		return this;
	}
	
	public PicklistPageAndroid validateRoutePlaceTypeDisplayed() {
		List<String> assetIDRouteTypePlace=new ScheduledAssetPageAndroid().getAssetIDRouteTypePlace();
//		eleIsDisplayedAccessibilityID(routeData);
//		eleIsDisplayedAccessibilityID(placeData);
//		eleIsDisplayedAccessibilityID(typeData);
		
		for (String val : assetIDRouteTypePlace) {
			eleIsDisplayedAccessibilityID(val);
		}
		return this;
	}

	public PicklistPageAndroid checkMarkNotDisplayed() {
		if(getResourceID("xpath", picklistXpath).contains("tick: false")) {
			System.out.println("Pick List icon does not have completed checkmark");
		}
		return this;
	}

	public PicklistPageAndroid checkMarkDisplayed() {
		if(getResourceID("xpath", picklistXpath).contains("tick: true")) {
			System.out.println("Pick List icon has completed checkmark");
		}
		return this;
	}

	public PicklistPageAndroid clickOnBackBtnInPicklist() {
		click("xpath", backPicklistXpath);
		return this;
	}
	
	public PicklistPageAndroid validateCoilDetailsDisplay() {
//		String coil1DetailsFromApp=getResourceID("xpath", coil1XPath);
//		if(coil1DetailsFromApp.equalsIgnoreCase(coil1Data)) {
//			System.out.println(coil1Data+" coil details is displayed as expected");
//		}
//		String coil2DetailsFromApp=getResourceID("xpath", coil2XPath);
//		if(coil2DetailsFromApp.equalsIgnoreCase(coil2Data)) {
//			System.out.println(coil2Data+" coil details is displayed as expected");
//		}
//		String coil3DetailsFromApp=getResourceID("xpath", coil3XPath);
//		if(coil3DetailsFromApp.equalsIgnoreCase(coil3Data)) {
//			System.out.println(coil3Data+" coil details is displayed as expected");
//		}
//		String coil4DetailsFromApp=getResourceID("xpath", coil4XPath);
//		if(coil4DetailsFromApp.equalsIgnoreCase(coil4Data)) {
//			System.out.println(coil4Data+" coil details is displayed as expected");
//		}
		return this;
	}
	
	public List<String> extractCoilsDetails() {
		String coil1DetailsFromApp=getResourceID("xpath", coil1XPath);
		coilList.add(coil1DetailsFromApp);
		String coil2DetailsFromApp=getResourceID("xpath", coil2XPath);
		coilList.add(coil2DetailsFromApp);
		String coil3DetailsFromApp=getResourceID("xpath", coil3XPath);
		coilList.add(coil3DetailsFromApp);
		String coil4DetailsFromApp=getResourceID("xpath", coil4XPath);
		coilList.add(coil4DetailsFromApp);
		return coilList;
	}
	
	public List<String> extractCoilsNames() {
		String coil=coilList.toString();
		for (String cl : coilList) {
			String coi=cl.split(";")[0];
			coils.add(coi);
		}
		return coils;
	}
	
	
	
	public PicklistPageAndroid validateDexPriceMisMatch() {
//		eleIsDisplayedAccessibilityID(dexPriceMismatchErrorMsgData);
		return this;
	}

}
