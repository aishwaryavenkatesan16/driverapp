package androidPages;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.testng.asserts.Assertion;

import Pages.DataRead;
import mobileWrap.MobileWrapper;

public class RoutePageAndroid extends MobileWrapper {
	
	DataRead data = new DataRead();
	String sheetName = "Route";
	Assertion assertion = new Assertion();
	public static String platform;
	static List<String> rout=new ArrayList<String>();

	String routeText = "//android.widget.TextView[@content-desc=\"Route\"]";
	String operator = "(//android.widget.TextView[@content-desc=\"Text\"])[1]";
	String routesearch = "//android.widget.EditText[@content-desc=\"Item Search\"]";
	String routefilter = "(//android.widget.TextView[@content-desc=\"Text\"])[5]";
	String routeField="//android.widget.EditText[@content-desc=\"Item Search\"]";
	String routeFieldClearBtn="//android.widget.TextView[@content-desc=\"clear\"]";
	String invalidRoute="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[3]/android.widget.TextView";
	
	String austinRoute="//android.widget.TextView[@content-desc=\"10000-Austin Installs\"]";
	String oneZoneoute="//android.widget.TextView[@content-desc=\"101\"]";
	String oneZtworoute="//android.widget.TextView[@content-desc=\"102\"]";
	String oneZthreeroute="//android.widget.TextView[@content-desc=\"103-Test Rt MKTS\"]";
	String oneZfourroute="//android.widget.TextView[@content-desc=\"104\"]";
	String oneZsixroute="//android.widget.TextView[@content-desc=\"106\"]";
	String oneZnineroute="//android.widget.TextView[@content-desc=\"109\"]";
	String oneTwoTworoute="//android.widget.TextView[@content-desc=\"122\"]";
//	String covidRoute="//android.widget.TextView[@content-desc=\"Covid Route-PHX\"]";
//	String ptwotwoRoute="//android.widget.TextView[@content-desc=\"P-22\"]";
//	String paonefourRoute="//android.widget.TextView[@content-desc=\"P-AV14\"]";
	
	String rt1XPath = "//android.widget.TextView[@content-desc=\"Route\"][1]";
	String rt2XPath = "//android.widget.TextView[@content-desc=\"Route\"][2]";
	String rt3XPath = "//android.widget.TextView[@content-desc=\"Route\"][3]";
	
	
	String invalidRouteData = data.getCellData(sheetName, "InvalidRoute", 2);
	String routesListData = data.getCellData(sheetName, "RoutesList", 2);
	String validRouteData = data.getCellData(sheetName, "ValidRoute", 2);
	String routesListNotDisplayedData = data.getCellData(sheetName, "RoutesListNotDisplayed", 2);
	String invalidRouteErrorMsgData = data.getCellData(sheetName, "InvalidRouteErrorMsgData", 2);


	public RoutePageAndroid clickRoutefilter() {
		click("xpath", routefilter);

		return this;
	}

	public RoutePageAndroid VerifyInactiveRoutefilterDrpDwnPresent() {

		verifyText("JP nagar", "xpath", routefilter);

		return this;
	}

	public RoutePageAndroid VerifyActiveRoutefilterPresent(String route) {

		verifyText(route, "xpath", routefilter);

		return this;
	}

	public RoutePageAndroid enterSearchItem(String data) {
		enterValue(data, "xpath", routesearch);

		return this;
	}

	public RoutePageAndroid verifyRouteSearch() {
		eleIsDisplayed("xpath", routesearch);

		return this;
	}

	public RoutePageAndroid VerifyOperatorBackButton() {

		verifyText("Operator", "xpath", operator);

		return this;
	}

	public RoutePageAndroid VerifyRouteText() {

		eleIsDisplayedAccessibilityID("Route");

		return this;
	}
	
	public RoutePageAndroid clickOnRoute() {
		click("xpath", oneZnineroute);
//		clickAccessibilityID(rout.get(0));
		return this;
	}
	
	public RoutePageAndroid enterInvalidRoute() {
		enterValue(invalidRouteData, "xpath", routeField);
		return this;
	}

	public RoutePageAndroid validateInvalidateRouteErrorMsg() {
		if(getText("xpath", invalidRoute).equalsIgnoreCase(invalidRouteErrorMsgData)) {
			System.out.println("Invalid route error message is displayed as expected");
		}
		else {
			throw new RuntimeException("Invalid route error message is not displayed");
		}
//		eleIsDisplayed("xpath", invalidOperatorErrorMsg);
//		eleIsDisplayedAccessibilityID(invalidRouteErrorMsgData);
		return this;
	}

	public RoutePageAndroid clearRouteField() {
		click("xpath", routeFieldClearBtn);
		return this;
	}

	public RoutePageAndroid validateIfRoutesAreDisplayedAlphabeticalOrder() {
		ArrayList<String> beforeSortRouteAppList = new ArrayList<String>();
		for (String rt : routesListData.split(";")) {
			String s=getTextAccessibilityID(rt);
			beforeSortRouteAppList.add(s);
		}
		List<String> newRoute = new ArrayList<String>(beforeSortRouteAppList);
		Collections.sort(newRoute);
//		beforeSortOpAppList.add(getText("xpath",accentMainOpXpath));
//		beforeSortOpAppList.add(getText("xpath",allStarServicesOpXpath));
//		beforeSortOpAppList.add(getText("xpath",continentalCafeOpXpath));
//		beforeSortOpAppList.add(getText("xpath",gvcsOpXpath));
//		beforeSortOpAppList.add(getText("xpath",tomdraOpXpath));
//		ArrayList<String> afterSortOpAppList = new ArrayList<String>();
//		afterSortOpAppList.add(getText("xpath",accentMainOpXpath));
//		afterSortOpAppList.add(getText("xpath",allStarServicesOpXpath));
//		afterSortOpAppList.add(getText("xpath",continentalCafeOpXpath));
//		afterSortOpAppList.add(getText("xpath",gvcsOpXpath));
//		afterSortOpAppList.add(getText("xpath",tomdraOpXpath));
////		List<String> beforeSortOpList = Arrays.asList(operatorsListData.split(","));
////		List<String> opList = Arrays.asList(operatorsListData.split(","));
//		Collections.sort(afterSortOpAppList);
		if (beforeSortRouteAppList.equals(newRoute)) {
			System.out.println("Routes list is displayed in alphabetical order");
		} else {
			throw new RuntimeException("Routes list is not displayed in alphabetical order");
		}
		return this;
	}
	
	public RoutePageAndroid enterValidRoute() {
		enterValue(validRouteData, "xpath", routeField);
		return this;
	}
	
	public RoutePageAndroid validateIfListNarrowedDownOnRouteEntry(String route) {
		String routeXpath="";
		eleIsDisplayedAccessibilityID(route);
		List<String> routeListNotDisplayed = Arrays.asList(routesListNotDisplayedData.split(";"));
		for (String rt : routeListNotDisplayed) {
			switch (rt) { 
			case "10000-Austin Installs":
				routeXpath=austinRoute;
				
				break;
			case "101":
				routeXpath=oneZoneoute;
				
				break;
			case "102":
				routeXpath=oneZtworoute;
				
				break;
			case "103-Test Rt MKTS":
				routeXpath=oneZthreeroute;
				
				break;
			case "104":
				routeXpath=oneZfourroute;
				
				break;
			case "106":
				routeXpath=oneZsixroute;
				
				break;
			case "122":
				routeXpath=oneTwoTworoute;
				
				break;
//			case "Covid Route-PHX":
//				routeXpath=covidRoute;
//				
//				break;
////			case "P-22":
////				routeXpath=ptwotwoRoute;
////				
////				break;
//			case "P-AV14":
//				routeXpath=paonefourRoute;
//				
//				break;

			default:
				break;
			}
			
			eleIsNotDisplayed(routeXpath);
		}
		return this;
	}
	
	
	
	public RoutePageAndroid validateIfRouteValueIsCleared() {
		click("xpath", routeFieldClearBtn);
		verifyText("", "xpath", routeField);
		return this;
	}
	
	public RoutePageAndroid validateScheduledAssetScreenDisplayed(String screenName) {
		String[] screen=screenName.split(" ");
		for (String sc : screen) {
			eleIsDisplayedAccessibilityID(sc);
		}
		
		return this;
	}
	
	public RoutePageAndroid extractListOfRoutes() {
		rout.add(getText("xpath", rt1XPath));
		rout.add(getText("xpath", rt2XPath));
		rout.add(getText("xpath", rt3XPath));
//		int rtSize=sizeOfList("Route");
//		for (int i=1;i<rtSize;i++) {
//			rout.add(getTextAccessibilityID("Route"));
//			
//		}
		return this;
	}

}
