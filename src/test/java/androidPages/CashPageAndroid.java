package androidPages;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import javax.management.RuntimeErrorException;

import org.testng.asserts.Assertion;
import org.testng.log4testng.Logger;

import Pages.DataRead;
import mobileWrap.MobileWrapper;
import net.bytebuddy.dynamic.scaffold.MethodRegistry.Handler.ForAbstractMethod;

public class CashPageAndroid extends MobileWrapper {

	DataRead data = new DataRead();
	String sheetName = "Cash";
	Assertion assertion = new Assertion();
	public static String platform;

	String coil1XPath = "(//android.widget.TextView[@content-desc=\"Coils\"])[1]";
	String coil2XPath = "(//android.widget.TextView[@content-desc=\"Coils\"])[2]";
	String coil3XPath = "(//android.widget.TextView[@content-desc=\"Coils\"])[3]";
	String coil4XPath = "(//android.widget.TextView[@content-desc=\"Coils\"])[4]";
	String cashXpath = "//android.widget.TextView[@content-desc=\"Cash\"]";

	/******* Test Data from Excel sheet ********/

	String cashHeaders = data.getCellData(sheetName, "CashHeaders", 2);
	
	String bagActualData = data.getCellData(sheetName, "Bag Actual", 2);
	String coilRefillActualData = data.getCellData(sheetName, "Coin Refill Actual", 2);
	String recyclerRefillActualData = data.getCellData(sheetName, "Recycler Refill Actual", 2);
	String refundActualData = data.getCellData(sheetName, "Refund Actual", 2);
	String testVendActualData = data.getCellData(sheetName, "Test Vend Actual", 2);
	String salesMeterActualData = data.getCellData(sheetName, "Sales Meter Actual", 2);

	private static Logger LOGGER;

	public CashPageAndroid validateCashHeadersDisplayed() {
		for (String cashHeader : cashHeaders.split(";")) {
			eleIsDisplayedAccessibilityID(cashHeader);
		}
		return this;
	}

	public CashPageAndroid clickOnElement(String icon) {
		clickAccessibilityID(icon);
		return this;
	}

	public CashPageAndroid validatecharLimitsOfAllFields() {
		String val="";
		for (String cashHeader : cashHeaders.split(";")) {
			switch (cashHeader) {
			case "Bag":
				clickAccessibilityID(cashHeader);
				for (int i=1;i<9;i++) {
					clickAccessibilityID(String.valueOf(i));
					
				}
//				enterValueAccessibilityID(cashHeader, bagData);
				val=getTextAccessibilityID(cashHeader);
				if(val.length()==8) {
					System.out.println(cashHeader+" can accept only 8 characters and it is validated successfully");
				}
				break;
			case "Coin Refill":
				clickAccessibilityID(cashHeader);
				for (int i=1;i<9;i++) {
					clickAccessibilityID(String.valueOf(i));
					
				}
				val=getTextAccessibilityID(cashHeader);
				if(val.length()==7) {
					System.out.println(cashHeader+" can accept only 7 characters and it is validated successfully");
				}
				break;
			case "Recycler Refill":
				clickAccessibilityID(cashHeader);
				for (int i=1;i<9;i++) {
					clickAccessibilityID(String.valueOf(i));
					
				}
//				enterValueAccessibilityID(cashHeader, recyclerRefillData);
				val=getTextAccessibilityID(cashHeader);
				if(val.length()==7) {
					System.out.println(cashHeader+" can accept only 7 characters and it is validated successfully");
				}
				break;

			case "Refund":
				clickAccessibilityID(cashHeader);
				for (int i=1;i<3;i++) {
					clickAccessibilityID(String.valueOf(i));
					
				}
//				enterValueAccessibilityID(cashHeader, refundData);
				val=getTextAccessibilityID(cashHeader);
				if(val.length()==2) {
					System.out.println(cashHeader+" can accept only 2 characters and it is validated successfully");
				}
				break;
			case "Test Vend":
				clickAccessibilityID(cashHeader);
				for (int i=1;i<9;i++) {
					clickAccessibilityID(String.valueOf(i));
					
				}
//				enterValueAccessibilityID(cashHeader, testVendData);
				val=getTextAccessibilityID(cashHeader);
				if(val.length()==7) {
					System.out.println(cashHeader+" can accept only 7 characters and it is validated successfully");
				}
				break;
			case "Sales Meter":
				clickAccessibilityID(cashHeader);
				for (int i=1;i<=9;i++) {
					clickAccessibilityID(String.valueOf(i));
					
				}
				clickAccessibilityID("1");
//				enterValueAccessibilityID(cashHeader, salesMeterData);
				val=getTextAccessibilityID(cashHeader);
				if(val.length()==9) {
					System.out.println(cashHeader+" can accept only 9 characters and it is validated successfully");
				}
				break;

			default:
				break;
			}
		}
		return this;
	}
	
	public CashPageAndroid validateDataIsSavedForAllFields() {
		String val="";
		for (String cashHeader : cashHeaders.split(";")) {
			switch (cashHeader) {
			case "Bag":
				val=getTextAccessibilityID(cashHeader);
				if(val.equalsIgnoreCase(bagActualData)) {
					System.out.println("Data entered in "+cashHeader+" is saved and validated successfully");
				}
				else {
					throw new RuntimeException("Data entered in "+cashHeader+" is not saved");
				}
				break;
			case "Coin Refill":
				val=getTextAccessibilityID(cashHeader);
				if(val.equalsIgnoreCase(coilRefillActualData)) {
					System.out.println("Data entered in "+cashHeader+" is saved and validated successfully");
				}
				else {
					throw new RuntimeException("Data entered in "+cashHeader+" is not saved");
				}
				break;
			case "Recycler Refill":
				val=getTextAccessibilityID(cashHeader);
				if(val.equalsIgnoreCase(recyclerRefillActualData)) {
					System.out.println("Data entered in "+cashHeader+" is saved and validated successfully");
				}
				else {
					throw new RuntimeException("Data entered in "+cashHeader+" is not saved");
				}
				break;

			case "Refund":
				val=getTextAccessibilityID(cashHeader);
				if(val.equalsIgnoreCase(refundActualData)) {
					System.out.println("Data entered in "+cashHeader+" is saved and validated successfully");
				}
				else {
					throw new RuntimeException("Data entered in "+cashHeader+" is not saved");
				}
				break;
			case "Test Vend":
				val=getTextAccessibilityID(cashHeader);
				if(val.equalsIgnoreCase(testVendActualData)) {
					System.out.println("Data entered in "+cashHeader+" is saved and validated successfully");
				}
				else {
					throw new RuntimeException("Data entered in "+cashHeader+" is not saved");
				}
				break;
			case "Sales Meter":
				val=getTextAccessibilityID(cashHeader);
				if(val.equalsIgnoreCase(salesMeterActualData)) {
					System.out.println("Data entered in "+cashHeader+" is saved and validated successfully");
				}
				else {
					throw new RuntimeException("Data entered in "+cashHeader+" is not saved");
				}
				break;

			default:
				break;
			}
		}
		return this;
	}
	
	public CashPageAndroid validateRoutePlaceTypeDisplayed() {
		List<String> assetIDRouteTypePlace=new ScheduledAssetPageAndroid().getAssetIDRouteTypePlace();
//		eleIsDisplayedAccessibilityID(routeData);
//		eleIsDisplayedAccessibilityID(placeData);
//		eleIsDisplayedAccessibilityID(typeData);
		
		for (String val : assetIDRouteTypePlace) {
			eleIsDisplayedAccessibilityID(val);
		}
		return this;
	}

	public CashPageAndroid checkMarkNotDisplayed() {
		if(getResourceID("xpath", cashXpath).contains("tick: false")) {
			System.out.println("Pick List icon does not have completed checkmark");
		}
		return this;
	}

	public CashPageAndroid checkMarkDisplayed() {
		if(getResourceID("xpath", cashXpath).contains("tick: true")) {
			System.out.println("Cash icon has completed checkmark");
		}
		return this;
	}

}
