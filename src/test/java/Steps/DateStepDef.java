package Steps;

import org.openqa.selenium.JavascriptExecutor;

import androidPages.DatePageAndroid;
import androidPages.LoginLogoutPageAndroid;
import androidPages.RoutePageAndroid;
import androidPages.OperatorPage;
import iOSPages.LoginLogoutPageiOS;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import mobileWrap.MobileWrapper;

public class DateStepDef extends MobileWrapper {
	public static String osVersion;
	public static String functional;
	JavascriptExecutor jse;

	
	@Then("validate if Date page is displayed for {string}")
	public void validate_if_date_page_is_displayed(String OSVersion) {
		osVersion = OSVersion;
		try {
			if (osVersion.equalsIgnoreCase("iOS")) {
			} else if (osVersion.equalsIgnoreCase("android")) {
				new DatePageAndroid().VerifyDateText();
			}
		} catch (Exception e) {
			closeApp(osVersion);
			e.printStackTrace();
		}
	}
	
	@And("validate if {string} and {string} buttons are displayed")
	public void validate_if_start_view_are_displayed(String start,String view) {
		try {
			if (osVersion.equalsIgnoreCase("iOS")) {
			} else if (osVersion.equalsIgnoreCase("android")) {
				new DatePageAndroid().verifyStartViewBtnsDsiplayed();
			}
		} catch (Exception e) {
			closeApp(osVersion);
			e.printStackTrace();
		}
	}
	
	@And("validate if dates are displayed in ascending order")
	public void validate_if_dates_displayed_ascending_order() {
		try {
			if (osVersion.equalsIgnoreCase("iOS")) {
			} else if (osVersion.equalsIgnoreCase("android")) {
				new RoutePageAndroid().VerifyRouteText();
			}
		} catch (Exception e) {
			closeApp(osVersion);
			e.printStackTrace();
		}
	}
	
//	@When("I click on {string} button")
//	public void i_click_on_button(String startOrView) {
//		try {
//			if (osVersion.equalsIgnoreCase("iOS")) {
//			} else if (osVersion.equalsIgnoreCase("android")) {
//				new RoutePageAndroid().VerifyRouteText();
//			}
//		} catch (Exception e) {
//			closeApp(osVersion);
//			e.printStackTrace();
//		}
//	}
	
	@Then("validate if Scheduled Asset page is displayed and {string} action can be performed")
	public void validate_if_scheduled_asset_page_is_displayed_action_performed(String anyOrNo) {
		try {
			if (osVersion.equalsIgnoreCase("iOS")) {
			} else if (osVersion.equalsIgnoreCase("android")) {
				new RoutePageAndroid().VerifyRouteText();
			}
		} catch (Exception e) {
			closeApp(osVersion);
			e.printStackTrace();
		}
	}
	
	@And("I navigate back to Date screen")
	public void i_navigate_back_to_date_screen() {
		try {
			if (osVersion.equalsIgnoreCase("iOS")) {
			} else if (osVersion.equalsIgnoreCase("android")) {
				new RoutePageAndroid().VerifyRouteText();
			}
		} catch (Exception e) {
			closeApp(osVersion);
			e.printStackTrace();
		}
	}
	
	

}
