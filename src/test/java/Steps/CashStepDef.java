package Steps;

import org.openqa.selenium.JavascriptExecutor;

import androidPages.CashPageAndroid;
import androidPages.LoginLogoutPageAndroid;
import androidPages.RoutePageAndroid;
import androidPages.ServiceInvPageAndroid;
import androidPages.OperatorPage;
import androidPages.PicklistPageAndroid;
import iOSPages.LoginLogoutPageiOS;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import mobileWrap.MobileWrapper;

public class CashStepDef extends MobileWrapper {
	public static String osVersion;
	public static String functional;
	JavascriptExecutor jse;

	@When("I click on {string} icon for {string}")
	public void i_click_on_cash_screen_for_OS(String icon,String OSVersion) {
		osVersion = OSVersion;
		try {
			if (osVersion.equalsIgnoreCase("iOS")) {
			} else if (osVersion.equalsIgnoreCase("android")) {
				new CashPageAndroid().clickOnElement(icon);
			}
		} catch (Exception e) {
			closeApp(osVersion);
			e.printStackTrace();
		}
	}

	@And("validate if cash headers are displayed")
	public void validate_if_cash_headers_are_displayed() {
		try {
			if (osVersion.equalsIgnoreCase("iOS")) {
			} else if (osVersion.equalsIgnoreCase("android")) {
				new CashPageAndroid().validateCashHeadersDisplayed();
			}
		} catch (Exception e) {
			closeApp(osVersion);
			e.printStackTrace();
		}
	}

	@Then("validate the character limits of all fields")
	public void validate_character_limits_of_all_fields() {
		try {
			if (osVersion.equalsIgnoreCase("iOS")) {
			} else if (osVersion.equalsIgnoreCase("android")) {
				new CashPageAndroid().validatecharLimitsOfAllFields();
			}
		} catch (Exception e) {
			closeApp(osVersion);
			e.printStackTrace();
		}
	}

	

	@Then("validate data is saved for all fields in cash page")
	public void validate_if_data_saved_for_all_fields_in_cash_page() {
		try {
			if (osVersion.equalsIgnoreCase("iOS")) {
			} else if (osVersion.equalsIgnoreCase("android")) {
				new CashPageAndroid().validateDataIsSavedForAllFields();
			}
		} catch (Exception e) {
			closeApp(osVersion);
			e.printStackTrace();
		}
	}
	
	@Then("I click on {string} button in cash screen")
	public void i_click_on_button_in_cash_screen(String buttonName) {
		try {
			if (osVersion.equalsIgnoreCase("iOS")) {
			} else if (osVersion.equalsIgnoreCase("android")) {
				new CashPageAndroid().clickOnElement(buttonName);
			}
		} catch (Exception e) {
			closeApp(osVersion);
			e.printStackTrace();
		}
	}
	
	@Then("validate if route place and type is displayed in Cash screen")
	public void validate_if_route_and_place_and_type_is_displayed_in_cash_screen() {
		try {
			if (osVersion.equalsIgnoreCase("iOS")) {
			} else if (osVersion.equalsIgnoreCase("android")) {
				new CashPageAndroid().validateRoutePlaceTypeDisplayed();
			}
		} catch (Exception e) {
			closeApp(osVersion);
			e.printStackTrace();
		}
	}
	
	@And("validate if {string} icon has {string} mark on all fields entry")
	public void validate_if_icon_has_mark(String icon, String compOrNoCompMark) {
		try {
			if (osVersion.equalsIgnoreCase("iOS")) {
			} else if (osVersion.equalsIgnoreCase("android")) {
				if (compOrNoCompMark.equalsIgnoreCase("not completed")) {
					new CashPageAndroid().checkMarkNotDisplayed();
				} else if (compOrNoCompMark.equalsIgnoreCase("completed")) {
					new CashPageAndroid().checkMarkDisplayed();

				}
			}
		} catch (Exception e) {
			closeApp(osVersion);
			e.printStackTrace();
		}
	}

	
}
