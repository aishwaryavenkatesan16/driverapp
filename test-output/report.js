$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("file:src/test/java/Features/ServicePlanogram.feature");
formatter.feature({
  "name": "Service Planogram flow of DriverApp",
  "description": "",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@driverAppServicePlanogramFlow"
    }
  ]
});
formatter.scenarioOutline({
  "name": "Service Planogram flow validations",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@servicePlanogramFlow"
    }
  ]
});
formatter.step({
  "name": "I launch the application in \"\u003cOSVersion\u003e\" for \"\u003cFunctionality\u003e\"",
  "keyword": "Given "
});
formatter.step({
  "name": "I click on driver app to access location",
  "keyword": "Then "
});
formatter.step({
  "name": "I click on driver app to access media",
  "keyword": "And "
});
formatter.step({
  "name": "I enter \"\u003cusername\u003e\" and \"\u003cpassword\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "I click login button",
  "keyword": "And "
});
formatter.step({
  "name": "I verify operator text in \"\u003cOSVersion\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "I click on operator",
  "keyword": "Then "
});
formatter.step({
  "name": "I click on route in \"\u003cOSVersion\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "I extract asset ID type and place from Scheduled asset screen for \"\u003cOSVersion\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "I click on asset in scheduled asset screen for \"\u003cOSVersion\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "I click on \"Pick List\" icon",
  "keyword": "When "
});
formatter.step({
  "name": "validate if route place and type is displayed in Pick list screen for \"\u003cOSVersion\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "I extract coils details from Picklist screen",
  "keyword": "And "
});
formatter.step({
  "name": "I extract coil names from Picklist screen to validate in Service Inventory screen",
  "keyword": "And "
});
formatter.step({
  "name": "I navigate back to select activity screen from Pick list screen",
  "keyword": "And "
});
formatter.step({
  "name": "I navigate to \"Service/Inventory\" screen for \"\u003cOSVersion\u003e\"",
  "keyword": "When "
});
formatter.step({
  "name": "I navigate back to select activity screen from Service Inv screen",
  "keyword": "Then "
});
formatter.step({
  "name": "I click on \"Okay\" in Service Inventory screen",
  "keyword": "Then "
});
formatter.step({
  "name": "validate coil details for each coil displayed",
  "keyword": "And "
});
formatter.step({
  "name": "validate fields dispalyed for each coil",
  "keyword": "And "
});
formatter.step({
  "name": "I navigate back to select activity screen from Service Inv screen",
  "keyword": "Then "
});
formatter.step({
  "name": "I click on \"Cancel\" in Service Inventory screen",
  "keyword": "Then "
});
formatter.step({
  "name": "Update execution status for \"\u003cFunctionality\u003e\" functionality",
  "keyword": "And "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "OSVersion",
        "username",
        "password",
        "Operator",
        "Route",
        "Functionality"
      ]
    },
    {
      "cells": [
        "android",
        "skaushik@cantaloupe.com",
        "Saurabh@123",
        "Accent - Main HQ (TX)",
        "109",
        "servicePlanogramPageFunctionality"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Service Planogram flow validations",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@driverAppServicePlanogramFlow"
    },
    {
      "name": "@servicePlanogramFlow"
    }
  ]
});
formatter.step({
  "name": "I launch the application in \"android\" for \"servicePlanogramPageFunctionality\"",
  "keyword": "Given "
});
formatter.match({
  "location": "Steps.LoginLogoutStepDef.i_launch_the_application_in_version_forfunctionality(java.lang.String,java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.afterstep({
  "status": "passed"
});
formatter.step({
  "name": "I click on driver app to access location",
  "keyword": "Then "
});
formatter.match({
  "location": "Steps.LoginLogoutStepDef.i_click_on_driver_app_to_access_location()"
});
formatter.result({
  "status": "passed"
});
formatter.afterstep({
  "status": "passed"
});
formatter.step({
  "name": "I click on driver app to access media",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.LoginLogoutStepDef.i_click_on_driver_app_to_access_media()"
});
formatter.result({
  "status": "passed"
});
formatter.afterstep({
  "status": "passed"
});
formatter.step({
  "name": "I enter \"skaushik@cantaloupe.com\" and \"Saurabh@123\"",
  "keyword": "Then "
});
formatter.match({
  "location": "Steps.LoginLogoutStepDef.i_enter_username_and_password(java.lang.String,java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.afterstep({
  "status": "passed"
});
formatter.step({
  "name": "I click login button",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.LoginLogoutStepDef.i_click_login_button()"
});
formatter.result({
  "status": "passed"
});
formatter.afterstep({
  "status": "passed"
});
formatter.step({
  "name": "I verify operator text in \"android\"",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.OperatorStepDef.i_verify_operator_text_in(java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.afterstep({
  "status": "passed"
});
formatter.step({
  "name": "I click on operator",
  "keyword": "Then "
});
formatter.match({
  "location": "Steps.OperatorStepDef.i_click_on_operator()"
});
formatter.result({
  "status": "passed"
});
formatter.afterstep({
  "status": "passed"
});
formatter.step({
  "name": "I click on route in \"android\"",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.RouteStepDef.i_click_on_route(java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.afterstep({
  "status": "passed"
});
formatter.step({
  "name": "I extract asset ID type and place from Scheduled asset screen for \"android\"",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.ScheduledAssetStepDef.i_extract_asset_ID_type_and_place_from_Scheduled_asset_screen(java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.afterstep({
  "status": "passed"
});
formatter.step({
  "name": "I click on asset in scheduled asset screen for \"android\"",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.ScheduledAssetStepDef.i_click_on_asset_in_scheduled_asset_screen_for_OS(java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.afterstep({
  "status": "passed"
});
formatter.step({
  "name": "I click on \"Pick List\" icon",
  "keyword": "When "
});
formatter.match({
  "location": "Steps.ScheduledAssetStepDef.i_click_on_icon(java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.afterstep({
  "status": "passed"
});
formatter.step({
  "name": "validate if route place and type is displayed in Pick list screen for \"android\"",
  "keyword": "Then "
});
formatter.match({
  "location": "Steps.PicklistStepDef.validate_if_route_and_place_and_type_is_displayed_in_Pick_list_screen(java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.afterstep({
  "status": "passed"
});
formatter.step({
  "name": "I extract coils details from Picklist screen",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.PicklistStepDef.i_extract_coils_details_from_Picklist_screen()"
});
formatter.result({
  "status": "passed"
});
formatter.afterstep({
  "status": "passed"
});
formatter.step({
  "name": "I extract coil names from Picklist screen to validate in Service Inventory screen",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.PicklistStepDef.i_extract_coil_names_from_Picklist_screen_to_validate_in_Service_Inventory_screen()"
});
formatter.result({
  "status": "passed"
});
formatter.afterstep({
  "status": "passed"
});
formatter.step({
  "name": "I navigate back to select activity screen from Pick list screen",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.PicklistStepDef.i_navigate_back_to_select_activity_screen()"
});
formatter.result({
  "status": "passed"
});
formatter.afterstep({
  "status": "passed"
});
formatter.step({
  "name": "I navigate to \"Service/Inventory\" screen for \"android\"",
  "keyword": "When "
});
formatter.match({
  "location": "Steps.ServiceInvStepDef.i_navigate_to_screen(java.lang.String,java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.afterstep({
  "status": "passed"
});
formatter.step({
  "name": "I navigate back to select activity screen from Service Inv screen",
  "keyword": "Then "
});
formatter.match({
  "location": "Steps.ServiceInvStepDef.i_navigate_back_to_select_activity_screen()"
});
formatter.result({
  "status": "passed"
});
formatter.afterstep({
  "status": "passed"
});
formatter.step({
  "name": "I click on \"Okay\" in Service Inventory screen",
  "keyword": "Then "
});
formatter.match({
  "location": "Steps.ServiceInvStepDef.i_click_on_okay_or_cancel_in_Service_Inventory_screen(java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.afterstep({
  "status": "passed"
});
formatter.step({
  "name": "validate coil details for each coil displayed",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.ServiceInvStepDef.validate_coil_details_for_each_coil_displayed()"
});
formatter.result({
  "error_message": "java.lang.AssertionError: Element is not displayed\n\tat org.testng.Assert.fail(Assert.java:97)\n\tat org.testng.asserts.Assertion$6.doAssert(Assertion.java:136)\n\tat org.testng.asserts.Assertion.executeAssert(Assertion.java:25)\n\tat org.testng.asserts.Assertion.doAssert(Assertion.java:12)\n\tat org.testng.asserts.Assertion.fail(Assertion.java:132)\n\tat mobileWrap.MobileWrapper.getResourceID(MobileWrapper.java:1915)\n\tat androidPages.PicklistPageAndroid.extractCoilsDetails(PicklistPageAndroid.java:101)\n\tat androidPages.ServiceInvPageAndroid.validateCoilDetailsForEachCoil(ServiceInvPageAndroid.java:73)\n\tat Steps.ServiceInvStepDef.validate_coil_details_for_each_coil_displayed(ServiceInvStepDef.java:109)\n\tat ✽.validate coil details for each coil displayed(file:///Users/avenkatesan/Documents/Workspace/Current%20Project/driverapp/src/test/java/Features/ServicePlanogram.feature:26)\n",
  "status": "failed"
});
formatter.afterstep({
  "error_message": "org.openqa.selenium.NoSuchSessionException: Session ID is null. Using WebDriver after calling quit()?\nBuild info: version: \u00273.141.59\u0027, revision: \u0027e82be7d358\u0027, time: \u00272018-11-14T08:17:03\u0027\nSystem info: host: \u0027CHN-LAP-AISHWAR\u0027, ip: \u00272405:201:e038:7031:ddc1:4112:83c6:8465%en0\u0027, os.name: \u0027Mac OS X\u0027, os.arch: \u0027x86_64\u0027, os.version: \u002712.3.1\u0027, java.version: \u002717.0.2\u0027\nDriver info: driver.version: RemoteWebDriver\n\tat org.openqa.selenium.remote.HttpCommandExecutor.execute(HttpCommandExecutor.java:125)\n\tat io.appium.java_client.remote.AppiumCommandExecutor.execute(AppiumCommandExecutor.java:239)\n\tat org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:552)\n\tat io.appium.java_client.DefaultGenericMobileDriver.execute(DefaultGenericMobileDriver.java:46)\n\tat io.appium.java_client.AppiumDriver.execute(AppiumDriver.java:1)\n\tat io.appium.java_client.android.AndroidDriver.execute(AndroidDriver.java:1)\n\tat org.openqa.selenium.remote.RemoteWebDriver.getScreenshotAs(RemoteWebDriver.java:295)\n\tat Steps.Hooks.getByteScreenshot(Hooks.java:61)\n\tat Steps.Hooks.as(Hooks.java:27)\n",
  "status": "failed"
});
formatter.step({
  "name": "validate fields dispalyed for each coil",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.ServiceInvStepDef.validate_fields_dispalyed_for_each_coil()"
});
formatter.result({
  "status": "skipped"
});
formatter.afterstep({
  "status": "skipped"
});
formatter.step({
  "name": "I navigate back to select activity screen from Service Inv screen",
  "keyword": "Then "
});
formatter.match({
  "location": "Steps.ServiceInvStepDef.i_navigate_back_to_select_activity_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.afterstep({
  "status": "skipped"
});
formatter.step({
  "name": "I click on \"Cancel\" in Service Inventory screen",
  "keyword": "Then "
});
formatter.match({
  "location": "Steps.ServiceInvStepDef.i_click_on_okay_or_cancel_in_Service_Inventory_screen(java.lang.String)"
});
formatter.result({
  "status": "skipped"
});
formatter.afterstep({
  "status": "skipped"
});
formatter.step({
  "name": "Update execution status for \"servicePlanogramPageFunctionality\" functionality",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.LoginLogoutStepDef.update_execution_status_for_functionality(java.lang.String)"
});
formatter.result({
  "status": "skipped"
});
formatter.afterstep({
  "status": "skipped"
});
formatter.after({
  "status": "passed"
});
});